"""A very basic proto version of the service that lives on core python and uses all needed modules.

Notes on the prototype, to describe what is not here/simulated:

This is a simple command line init, to illustrate the use of the system in place, it can be modified
in order to change the behavior, provided the init lines stay the same (or are adjusted for).

The system is using a database in sqlite3, this can be replaced with any more complex db depending
on needs, also all of the database access is living on one accessor which is only for simlicities
sake, ideally inventory and quadrotor control would not be on the same server/db.

The system uses Simulated quadrotors, which use an added up ord of the name of the location to
determine the distance they fly, as such this is not a realistic simulation of flight times/gps
tracking, or anything of the sort.

The system uses one message queue to process all database traffic, this can be replaced with a pool,
if the weight of interactions on the inventory is expected to be extra high, but would require a
little extra sync code.

This system does not have a single point outward facing API, i did this on purpose for illustration,
but if one is needed the functions which provide order placement, canceling and inventory queries
would be put in one interface class and interacted with.

There is no configuration management, and everything is done through CONST style definitions,
ideally there would be a model view description.

There are no unit tests, but having a testing background I itch to add some... though I am
refraining to stick to the prompt.

Lastly, this system would ideally work in sync with an AsyncServer or Django or the like, as opposed
to a direct API connection such as this file.

Copyright Alexei Kozlenok 2015.
"""
import logging
import time
from qadlivery_core.inventory import Inventory
from qadlivery_core.orders import Orders
from qadlivery_core.quad_sim import QuadSim
from qadlivery_core.quads import Quads
from qadlivery_core.sqlite_accessor import SqliteAccessor

__author__ = 'aselus'


def main():
  logging.basicConfig(level=logging.INFO)
  QuadSim(1).Init()
  QuadSim(2).Init()
  QuadSim(3).Init()

  main_db = SqliteAccessor()
  main_db.Init()
  inventory = Inventory(main_db)
  quads = Quads(main_db, inventory)
  quads.Init()

  def LogStates(order, state):
    logging.info("State changed for order [%s]: %s", order.order_id, state)

  Orders.PlaceOrder(main_db, inventory, quads, 1, "a", callbacks={"general":LogStates})

  time.sleep(5)
  quads.Shutdown()
  main_db.Shutdown()
  QuadSim.ShutdownAllSimulations()

if __name__ == "__main__":
    main()
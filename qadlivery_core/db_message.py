"""Simple handler class for containing priority messages.
"""
import threading

__author__ = 'aselus'

class DbMessage(object):
  DEFAULT_PRIORITY = 100

  def __init__(self, priority = None, message = None):
    """Construction of the SQL message complete with callback threaded event.

    :param priority: priority at which to execute the message.
    :param list message: message to be sent to the SQL for response, in wrapped format:
      i.e.:
      ["From table SELECT * WHERE id=? AND name=?", 1, "Roger"]
    """
    self.priority = priority or self.DEFAULT_PRIORITY
    self._message = message
    self.completion_event = threading.Event()
    self.execution_event = threading.Event()
    self.__result_data__ = None
    self.executing_database = None

  def __cmp__(self, other):
    """Compare override function, for priority checking in queue, compares based on priority var.

    :param other: DbMessage being compared against
    :return: comparison result against other items priority
    """
    return cmp(self.priority, other.priority)

  @property
  def message(self):
    return self._message

  @property
  def result_data(self):
    if self.completion_event.is_set():
      return self.__result_data__
    return None

  def WaitForResult(self):
    """Wait for the completion of this message."""
    self.completion_event.wait()
    return self.result_data

  def CheckForResult(self):
    """Check if this message has completed it's execution."""
    return self.completion_event.is_set()

  def Reset(self):
    """Reset this message so that it can be sent again."""
    self.completion_event.clear()
    self.__result_data__ = None

  def Rerun(self):
    """Re-execute the query again"""
    if self.executing_database:
      self.Reset()
      self.executing_database.ExecuteQuery(self)

  def Complete(self, data, database):
    """Completes the message, ready for parsing.

    :param data: result from query.
    """
    self.__result_data__ = data
    self.executing_database = database
    self.execution_event.clear()
    self.completion_event.set()


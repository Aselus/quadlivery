"""Module for managing quadrotor database access.

Copyright Alexei Kozlenok 2015.
"""
import Queue
import threading
import time
from qadlivery_core.db_message import DbMessage
from qadlivery_core.quad_controller import QuadController

__author__ = 'aselus'


class Quads(object):
  COLUMNS = [
    "id",
    "state",
    "order_id"
  ]

  STATES = [
    "parked",
    "delivering",
    "returning",
    "out_of_service"
  ]

  DEFAULT_TABLE = "quadrotors"

  def __init__(self, database, inventory, table_name=None):
    self.database = database
    self.inventory = inventory
    self.table_name = table_name or self.DEFAULT_TABLE
    self.need_a_quad = Queue.Queue()
    self.update_thread = threading.Thread(target=self.GetQuadThread)
    self.running_update_thread = threading.Event()

  def Init(self):
    """Initialize this database manager."""
    self.running_update_thread.set()
    self.update_thread.start()

  def Shutdown(self):
    """Gracefully exits the update thread once it's not acting."""
    self.running_update_thread.clear()
    self.update_thread.join()
    self.update_thread = threading.Thread(target=self.GetQuadThread)

  def GetQuad(self, order_id, item_id):
    """Gets a quad once one is available.

    :rtype : QuadController
    :param order_id: the id of the order for which the quad is to be allocated.
    :param item_id: the item which the quad will be carrying.
    :return: instance of the QuadController that will gain a quad allocation.
    """
    quad = QuadController(self, self.inventory, order_id,
                          self.GetAvailableQuad(order_id, item_id))
    self.need_a_quad.put(quad)
    return quad

  def GetQuadWithItem(self, order_id, item_id):
    """Gets a quad that has the requested item.

    :rtype : QuadController
    :param order_id: the id of the order for which the quad is to be allocated.
    :param item_id: the item which the quad will be carrying.
    :return: instance of the QuadController that will gain a quad allocation.
    """
    quad = QuadController(self, self.inventory, order_id,
                          self.AllocateQuadWithItem(order_id, item_id))
    quad.controlling_assigned.set()
    return quad

  def GetQuadThread(self):
    """Main Update loop for quad retrieval, for use by quad_controllers.

    This function pulls quad controllers that are waiting for a quad, and once a quad is available
    signals them that one has been allocated.
    """
    quad_controller = None
    while self.running_update_thread.is_set():
      if (not quad_controller and
          not self.need_a_quad.empty()):
        quad_controller = self.need_a_quad.get()
        if not quad_controller.is_cancelled.is_set():
          self.database.ExecuteQuery(quad_controller.quad_request_msg)
      elif quad_controller:
        if quad_controller.is_cancelled.is_set():
          quad_controller = None
        elif quad_controller.quad_request_msg.CheckForResult():
          if quad_controller.quad_request_msg.result_data:
            quad_controller.controlling_assigned.set()
            quad_controller = None
          else:
            time.sleep(1)
            quad_controller.quad_request_msg.Rerun()
      time.sleep(0.01)

  def GetAvailableQuad(self, for_order, item_id):
    """Request the purchase state of an item in stock.

    :param int for_order: order_id that needs the quad.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not a quad was allocated.
    """
    query = ["UPDATE %s SET order_id = ?, item_id = ? "
             "WHERE state = 'parked' and order_id = 0 "
             "ORDER BY id "
             "ASC LIMIT 1" % self.table_name,
             (for_order, item_id)
             ]
    message = DbMessage(message = query, priority = 50)
    return message

  def AllocateQuadWithItem(self, for_order, item_id):
    """Request the purchase state of an item in stock.

    :param int for_order: order_id that needs the quad.
    :param int item_id: item_id that is on a quad in transit.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not a quad was allocated.
    """
    query = ["UPDATE %s SET order_id = ? "
             "WHERE item_id = ? AND order_id = 0 "
             "ORDER BY id "
             "ASC LIMIT 1" % self.table_name,
             (for_order, item_id)
             ]
    message = DbMessage(message = query, priority = 10)
    self.database.ExecuteQuery(message)
    return message

  def GetQuadData(self, for_order):
    """Request the purchase state of an item in stock.

    :param int for_order: order_id that needs the quad.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain id of quad
    """
    query = ["SELECT * FROM %s WHERE order_id = ?" % self.table_name, (for_order, )]
    message = DbMessage(message = query)
    self.database.ExecuteQuery(message)
    return message

  def FreeFromOrder(self, quad_id):
    """Request the purchase state of an item in stock.

    :param int quad_id: id of the quad to be freed.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not the clear happened.
    """
    query = ["UPDATE %s SET order_id = 0 WHERE id = ?" % self.table_name, (quad_id, )]
    message = DbMessage(message = query, priority = 40)
    self.database.ExecuteQuery(message)
    return message

  def ChangeQuadState(self, state, quad_id):
    """Change the state of a quad in the db..

    :param int quad_id: id of the quad to be changed.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not the state change happened.
    """
    query = ["UPDATE %s SET state = ? WHERE id = ?" % self.table_name, (state, quad_id )]
    message = DbMessage(message = query)
    self.database.ExecuteQuery(message)
    return message

  def ClearQuadCarry(self, quad_id):
    """Clear the quads carried item.

    :param int quad_id: id of the quad to be changed.
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not the state change happened.
    """
    query = ["UPDATE %s SET item_id = 0 WHERE id = ?" % self.table_name, (quad_id, )]
    message = DbMessage(message = query, priority=10)
    self.database.ExecuteQuery(message)
    return message
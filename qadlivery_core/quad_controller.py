"""Module for controlling a quadrotor directly.

Copyright Alexei Kozlenok 2015.
"""
import threading
from qadlivery_core.quad_sim import QuadSim

__author__ = 'aselus'

class QuadController(object):
  def __init__(self, quads_manager, inventory, order_id, quad_request_msg, quad_id=0):
    self.quads_manager = quads_manager
    self.inventory = inventory
    self._quad_id = quad_id
    self.quad_row = None
    self.order_id = order_id
    self.lock = threading.RLock()
    self.quad_request_msg = quad_request_msg
    self.is_cancelled = threading.Event()
    self.controlling_assigned = threading.Event()
    self.delivered_event = threading.Event()
    self.quad = None

  @property
  def quad_id(self):
    """Gets the quad_id from the db if one has been assigned, otherwise displays known self id.

    :return: currently known quad_id
    """
    with self.lock:
      if self._quad_id == 0:
        if self.controlling_assigned.is_set():
          msg = self.quads_manager.GetQuadData(self.order_id)
          msg.WaitForResult()
          self.quad_row = msg.result_data
          self._quad_id = self.quad_row[0][0]
          self.quad = QuadSim.GetQuadrotor(self._quad_id)
      return self._quad_id

  def WaitForControlledQuadId(self):
    """Waits for a quad_id to be assigned, and then returns it.

    :return: Assigned quad_id
    """
    self.controlling_assigned.wait()
    return self.quad_id

  def CancelDelivery(self):
    """Cancels currently pending delivery."""
    self.is_cancelled.set()
    if self.quad_id:
      self.quad.CancelCurrentOrder()
      self._quad_id = 0
      self.quad = None

  def Deliver(self, location):
    """Deliver the currently allocated item with the currently allocated quad.

    :param location:
    """
    self.quad.AssignOrder(self.quads_manager, self.inventory, location, self, self._quad_id)

  def CheckDeliveryStatus(self):
    """Check for delivery status.

    :return: whether the delivery has occurred
    """
    return self.delivered_event.is_set()

"""Inventory management class, interacts with db to assist the user.

Copyright Alexei Kozlenok 2015.
"""
import logging
from qadlivery_core.db_message import DbMessage

__author__ = 'aselus'


class Inventory(object):
  COLUMNS = [
    "id",
    "name",
    "description",
    "stock",
    "returning_stock"
  ]

  DEFAULT_TABLE = "inventory"

  def __init__(self, database, table_name=None):
    """Constructor.

    :param SqliteAccessor database: database accessor which will be used to communicate with.
    :param str table_name: the name of the table
    """
    self._database = database
    self.table_name = table_name or self.DEFAULT_TABLE

  def RequestInventoryOfItems(self, value, column_type="id"):
    """Request the inventory state of an item

    :param value: the value for which to look inside of the database as the
    :param column_type:
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain id, name, description and total stock.
      i.e:
        [(2, u'item', u'b', 1), (3, u'item 2', u'c', 0)]
    """
    if column_type in self.COLUMNS:
      query = ["SELECT id, name, description, (stock + returning_stock) FROM %s "
               "WHERE %s = ?" % (self.table_name, column_type),
               (value, )]
      message = DbMessage(message = query)
      self._database.ExecuteQuery(message)
      return message

    else:
      logging.error("Unable to perform query, column type [%s] does not exist", column_type)
    return None

  def RequestPurchaseOfItem(self, item_id):
    """Request the purchase state of an item in stock.

    :param item_id: the value for which to look inside of the database as the
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not the item was allocated.
      i.e:
        0 - not allocated
        1 - allocated
    """
    query = ["UPDATE %s SET stock = stock - 1 WHERE id = ? AND stock > 0" % self.table_name,
             (item_id, )]
    message = DbMessage(message = query, priority = 50)
    self._database.ExecuteQuery(message)
    return message

  def RequestPurchaseOfCarriedItem(self, item_id):
    """Request the inventory state of an item that is being carried by a quadrotor.

    :param item_id: the value for which to look inside of the database as the
    :return DbMessage: Message which has been sent to be processed and will contain the results
      data once the query is completed. Query will contain whether or not the item was allocated.
      i.e:
        0 - not allocated
        1 - allocated
    """
    query = ["UPDATE %s SET returning_stock = returning_stock - 1 "
             "WHERE id = ? AND returning_stock > 0" % self.table_name,
             (item_id, )]
    message = DbMessage(message = query, priority = 50)
    self._database.ExecuteQuery(message)
    return message
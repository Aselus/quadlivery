"""Module for placing orders in their own threads[or as standalone]

usage to initiate an order[see function for arg list]:
  order = Orders.PlaceOrder(args)

usage to cancel an order:
  order.CancelOrder()
Copyright Alexei Kozlenok 2015.
"""
import threading
import uuid
import time

__author__ = 'aselus'


class Orders(object):

  STATES = [
    "canceled",
    "item_unavailable",
    "processing",
    "enroute",
    "delivered",
  ]

  def __init__(self, database, inventory, quads, item_id, location, order_id, callbacks=None):
    self.database = database
    self.inventory = inventory
    self.quads = quads
    self.order_id = order_id
    self.item_id = item_id
    self.location = location
    self.state = "processing"
    self.callbacks = callbacks or {}

  @classmethod
  def PlaceOrder(cls, database, inventory, quads, item_id, location, callbacks=None):
    """Places an Order for an item.

    :param database: database for use in storing this order record.
    :param inventory: inventory from which the items will be pulled.
    :param quads: quadrotor manager from which the quads will be allocated.
    :param item_id: id of the item being ordered.
    :param location: location for delivery.
    :param callbacks: dictionary of (state, function_pointer) sets that will get executed at
      state transition, or "general" for a callback that is always called at each state change.
      format example:
        function(order, state_name)
        callbacks = {"general":function}

      NOTE: all callbacks are blocking for the order thread.
    :return: order that has been initiated.
    """
    order_id = uuid.uuid1().hex
    order = cls(database, inventory, quads, item_id, location, order_id, callbacks)
    threading.Thread(target=order.OrderThread).start()
    return order

  def StateChange(self, state):
    """Preform a state change, and execute all appropriate callbacks.

    :param state: name of the new state.
    """
    if "general" in self.callbacks:
      self.callbacks["general"](self, state)
    if state in self.callbacks:
      self.callbacks[state](self, state)
    self.state = state

  def OrderThread(self):
    """Executes all the steps required to go all the way through an order.
    """
    quad = None
    self.StateChange("processing")
    msg = self.inventory.RequestPurchaseOfCarriedItem(self.item_id)
    msg.WaitForResult()

    if msg.result_data:
      quad = self.quads.GetQuadWithItem(self.order_id, self.item_id)
    else:
      msg = self.inventory.RequestPurchaseOfItem(self.item_id)
      msg.WaitForResult()
      if not msg.result_data:
        if self.state != "canceled":
          self.StateChange("item_unavailable")
        return

    if not quad:
      quad = self.quads.GetQuad(self.order_id, self.item_id)

    quad.WaitForControlledQuadId()
    quad.Deliver(self.location)

    self.StateChange("enroute")
    while not quad.CheckDeliveryStatus():
      if self.state == "canceled":
        quad.CancelDelivery()
        return
      time.sleep(0.01)
    self.StateChange("delivered")

  def GetState(self):
    """Returns the current state of the order."""
    return self.state

  def CancelOrder(self):
    """Cancel this order

    An order can only be canceled once it has been deemed valid, and is enroute.

    :return: whether the order was canceled successfully
    """
    if self.state == "processing" or self.state == "enroute":
      self.StateChange("canceled")
      return True

    else:
      return False

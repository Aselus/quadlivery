"""Manages database containing an inventory of items and the message queue to communicate with it.

Copyright Alexei Kozlenok 2015.
"""
import logging
import sqlite3
import Queue
import threading
import time

__author__ = 'aselus'


class SqliteAccessor(object):

  DEFAULT_DATABASE = "database.db"

  def __init__(self, database_address = None):
    self._connect_to_address = database_address or self.DEFAULT_DATABASE
    self._database_address = None
    self.connection = None
    self.cursor = None
    self.message_queue = Queue.PriorityQueue()
    self.running_update_thread = threading.Event()
    self.update_thread = threading.Thread(target=self.UpdateThread)

  def _Connect(self):
    """Attempts to connect to the queued connection server address.
    """
    try:
      connection = sqlite3.connect(self._connect_to_address)
      connection_to_close = self.connection

    except sqlite3.Error, e:
      logging.error("Unable to establish database connection with %s", self._connect_to_address)

    self.connection = connection
    try:
      self.cursor = connection.cursor()
    except sqlite3.Error, e:
      logging.error("Unable to establish a cursor for %s", self._connect_to_address)

    self._database_address = self._connect_to_address

    if connection_to_close:
      self._Disconnect(connection_to_close)

  def _Disconnect(self, connection_to_close = None):
    """ Disconnects currently connected connection, or one passed into the function.

    :param connection_to_close: the sqlite3 connection to close.
    """
    if not connection_to_close:
      connection_to_close = self.connection
      self.connection = None

    if connection_to_close:
      connection_to_close.commit()
      connection_to_close.close()
    else:
      logging.warning("Attempting to close a connection while none are open.")

  def ChangeDatabase(self, database_address):
    """Sets a new database for connection and then conencts.

    :param database_address: address of the new database to connect to.
    """
    self._connect_to_address = database_address
    self._Connect()

  def ExecuteQuery(self, message):
    """Execute a query in this db, on the message passed in.

    :param db_message.DbMessage message: Message to execute on.
    """
    if not message.execution_event.is_set():
      message.execution_event.set()
      self.message_queue.put(message)
    else:
      logging.error("Attempting to send a dbMessage that has already been processed.")


  def UpdateThread(self):
    """Update function that checks for message queue and acts on messages there when available."""
    self._Connect()
    while self.running_update_thread.is_set():
      if not self.message_queue.empty():
        message_to_process = self.message_queue.get()
        if type(message_to_process.message) is str:
          self.cursor.executescript(message_to_process.message)
        else:
          self.cursor.execute(*message_to_process.message)
        results = self.cursor.fetchall()
        if not results:
          results = self.cursor.rowcount
        message_to_process.Complete(results, self)
        self.connection.commit()
      else:
        time.sleep(0.1)
    self._Disconnect()

  def Init(self):
    """Initialize this database manager."""
    self.running_update_thread.set()
    self.update_thread.start()

  def Shutdown(self):
    """Gracefully exits the update thread once it's not acting."""
    self.running_update_thread.clear()
    self.update_thread.join()
    self.update_thread = threading.Thread(target=self.UpdateThread)


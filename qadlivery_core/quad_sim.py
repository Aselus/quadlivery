"""Module for simulating a quadrotor in a thread

Copyright Alexei Kozlenok 2015.
"""
import logging
import threading
import datetime
import time

__author__ = 'aselus'

class QuadSim(object):
  _quadrotors = {}

  @classmethod
  def GetQuadrotor(cls, quad_id):
    return cls._quadrotors[quad_id-1]

  @classmethod
  def ShutdownAllSimulations(cls):
    for inst in cls._quadrotors.iteritems():
      inst[1].Shutdown()

  def __new__(cls, *args, **kwargs):
    """Creates a new quadrotor simulation, adds it to the list and initiates it."""
    inst = super(QuadSim, cls).__new__(cls, *args, **kwargs)
    cls._quadrotors[len(cls._quadrotors)] = inst
    return inst

  def __init__(self, quad_id):
    self.running_update_thread = threading.Event()
    self.update_thread = threading.Thread(target=self.UpdateThread)
    self.quad_id = quad_id
    self.order_assigned = threading.Event()
    self.lock = threading.RLock()
    self.arrival_time = None
    self.quads_manager = None
    self.controller = None
    self.item_id = None
    self.state = "parked"

  def UpdateThread(self):
    """Update function that keeps the quadrotor buzzing."""
    while self.running_update_thread.is_set():
      with self.lock:
        if self.order_assigned.is_set():
          if self.arrival_time - datetime.datetime.now() < datetime.timedelta(0):
              self.quads_manager.ChangeQuadState("returning", self.quad_id)
              self.state = "returning"
              self.controller.delivered_event.set()
              self.quads_manager.ClearQuadCarry(self.quad_id)
              self.quads_manager.FreeFromOrder(self.quad_id)
              self.item_id = None
              self.SetArrivalTime("home")
              logging.info("%s has delivered an item returning home eta: %s.",
                           self.quad_id,
                           self.arrival_time)
              self.order_assigned.clear()
        else:
          if self.state == "returning":
            if self.arrival_time - datetime.datetime.now() < datetime.timedelta(0):
              if self.item_id:
                self.quads_manager.ClearQuadCarry(self.quad_id)
                self.item_id = None
              self.quads_manager.ChangeQuadState("parked", self.quad_id)
              self.state = "parked"
              logging.info("%s has parked at home.",
                           self.quad_id)

      time.sleep(0.01)

  def AssignOrder(self, quads_manager, inventory, location, controller, item_id):
    """Assign an order to be carried out by this quad.

    :param quads_manager: manager for quad db
    :param inventory: the inventory manager
    :param location: where the item is to be delivered
    :param controller: the controller which is in control of this quadrotor for the action.
    :param item_id: the id of the item being carried
    :return:
    """
    with self.lock:
      self.quads_manager = quads_manager
      self.controller = controller
      self.item_id = item_id
      quads_manager.ChangeQuadState("delivering", self.quad_id)
      self.state = "delivering"
      self.SetArrivalTime(location)
      logging.info("Quadrotor %s has a delivery set for %s", self.quad_id, self.arrival_time)
      self.order_assigned.set()

  def CancelCurrentOrder(self):
    """Cancels the currently active order."""
    with self.lock:
      self.quads_manager.ChangeQuadState("returning", self.quad_id)
      self.state = "returning"


  def SetArrivalTime(self, location):
    """Gets a destination time based on combined ord weight of the location string, for simulation.
    """
    dest_length = 0
    for character in location:
      dest_length += ord(character)
    self.arrival_time = datetime.datetime.now() + datetime.timedelta(milliseconds=dest_length)

  def Init(self):
    """Initialize this database manager."""
    self.running_update_thread.set()
    self.update_thread.start()

  def Shutdown(self):
    """Gracefully exits the update thread once it's not acting."""
    self.running_update_thread.clear()
    self.update_thread.join()
    self.update_thread = threading.Thread(target=self.UpdateThread)